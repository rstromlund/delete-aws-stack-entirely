// ==UserScript==
// @name         AWS Console Delete Stack
// @description  Repeatedly click on delete stack till current selections are gone
// @author       Rodney S
// @version      0.0.1
// @match        https://*.console.aws.amazon.com/cloudformation*
// @grant        none
// @run-at       document-end
// @namespace    https://gitlab.com/rstromlund/delete-aws-stack-entirely.git
// ==/UserScript==

(function() {
    'use strict';

    function addStyle(css) {
        const style = document.getElementById("awsDelButton") || (function() {
            const style = document.createElement('style');
            style.type = 'text/css';
            style.id = "awsDelButton";
            document.head.appendChild(style);
            return style;
        })();
        const sheet = style.sheet;
        sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
    }


    function delStack(it) {
        let i = it.next();
        if (! i) return;

        i.value.click();
        console.log('IT of ' + i);
        setTimeout(na => {
            let del = document.querySelectorAll('button.e2e-test-delete-stack-button');
            del[0].click();
            setTimeout(na => {
                let confrm = document.querySelectorAll('button.e2e-test-delete-modal-ok');
                confrm[0].click();
                setTimeout(delStack, 750, it);
            }, 500);
        }, 500);
    }

    setTimeout(na => {
        /*--- Create a button in a container div.  It will be styled and positioned with CSS. */
        var zNode = document.createElement('div');
        zNode.innerHTML = '<button id="myButton" type="button">Delete Stack!</button>';
        zNode.setAttribute('id', 'myContainer');
        document.body.appendChild(zNode);

        //--- Activate the newly added button.
        document.getElementById('myButton').addEventListener('click', ButtonClickAction, false);

        //--- Style our newly added elements using CSS.
        addStyle(`
#myContainer {
  position:   absolute;
  bottom:     0;
  left:       0;
  width:      10ex;
  height:     50px;
  font-size:  20px;
  background: orange;
  border:     3px outset black;
  margin:     5px;
  opacity:    0.9;
  z-index:    1100;
  padding:    5px 20px;
}
`);
        addStyle(`
#myButton {
  cursor:     pointer;
}
`);
        addStyle(`
#myContainer p {
  color:      red;
  background: white;
}
`);

        function ButtonClickAction(zEvent) {
            //let is = document.querySelectorAll('input.awsui-radio-native-input');
            let is = Array.from(document.querySelectorAll('input[type="radio"]')).filter(i => {
                return (i.id.startsWith("awsui-radio-button") || (i.className.startsWith("awsui_native-input_") && !i.id.startsWith("rollback"))) && i.clientWidth != 0
            });
            console.log('IS:' + is.length + is[0]);

            alert('Ok, here we go; deleting ' + is.length + ' stacks');
            setTimeout(delStack, 500, is.values());
        }

    }, 5000);

})();
