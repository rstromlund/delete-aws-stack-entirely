# delete aws stack entirely
Greasemonkey script

From the AWS console go to Cloud Formation, enter a filter if desired, click on "Delete Stack!" and this script will repeatedly click on delete stack and confirm till current selections are gone.
